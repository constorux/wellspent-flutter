# WellSpent
This is an app that aims to help you manage your personal finance. It will serve as a mobile companion for software such as GNUcash or Homebank. I want the user interface to be really intuitive to make it easy to add new expenses to your personal finance overview

### State of the project
I am currently working on building the most important components of the user interface. Parralel to that I noe started focusing on implementing the backends to integrate the app with existing personal finance softare such as GNUcash or Homebank. Currently i am working on getting the core features to work.

### Download
If you want to try the App for yourself please install the Flutter/Dart tools and download and import this project into Android Studio

### Contribute
If you want to contribute to the project feel free to contact me.