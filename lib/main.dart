import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart'
    show debugDefaultTargetPlatformOverride;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:well_spent/UI/startupPage.dart';
import 'package:well_spent/backend/WellSpent.dart';

void main() {
  // See https://github.com/flutter/flutter/wiki/Desktop-shells#target-platform-override
  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;

  runApp(new AppMainWidget());
}

final ThemeData kIOSTheme = new ThemeData(
    primarySwatch: Colors.orange,
    primaryColor: Colors.grey[100],
    primaryColorBrightness: Brightness.light,
    canvasColor: Colors.grey[200]);

final ThemeData kDefaultTheme = new ThemeData(
  primarySwatch: Colors.purple,
  accentColor: Colors.blueAccent[400],
  canvasColor: Colors.grey[200],
  //inputDecorationTheme: InputDecorationTheme(isCollapsed: true)
);

class AppMainWidget extends StatelessWidget {
  MaterialApp _app;
  StartupPage _defaultHome = new StartupPage();
  BuildContext _context;

  AppMainWidget() {
    WellSpent(this);
    _app = new MaterialApp(
      title: "WellSpent",
      theme: defaultTargetPlatform == TargetPlatform.iOS
          ? kIOSTheme
          : kDefaultTheme,
      home: _defaultHome,
      routes: <String, WidgetBuilder>{
        '/startup': (BuildContext context) => _defaultHome,
      },
    );
  }

  //TODO not at all reliable
  showToast(String message) {
    if (_defaultHome.context != null) {
      Scaffold.of(_defaultHome.context).showSnackBar(new SnackBar(
        content: new Text("Sending Message"),
      ));
    }
  }

  showPage(String name, Widget homeWidget, bool replace) {
    name = "/" + name;
    Map<String, WidgetBuilder> routesMap = {
      name: (BuildContext context) => homeWidget
    };
    _app.routes.addAll(routesMap);

    //_defaultHome = homeWidget;
    if (replace) {
      Navigator.of(_defaultHome.context).pushReplacementNamed(name);
      return;
    }
    Navigator.of(_defaultHome.context).pushNamed(name);
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return _app;
  }
}
