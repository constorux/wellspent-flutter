import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:well_spent/backend/financeObjects/Account.dart';
import 'package:well_spent/backend/financeObjects/Transaction.dart';

import 'SpendingOverview.dart';
import 'TransactionItem.dart';

class PaymentsOverview extends StatefulWidget {
  @override
  final ValueListenable<Account> accountListener;
  final ValueListenable<int> timeWindowListener;

  List<TransactionItem> transactionItems = new List<TransactionItem>();

  PaymentsOverview(this.accountListener, this.timeWindowListener);

  @override
  State<StatefulWidget> createState() {
    return PaymentsOverviewState();
  }
}

class PaymentsOverviewState extends State<PaymentsOverview>
    with SingleTickerProviderStateMixin {
  final List<Tab> myTabs = <Tab>[
    Tab(text: 'LAST PAYMENTS'),
    Tab(text: 'SPENDING OVERVIEW'),
  ];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: myTabs.length);

    widget.accountListener.addListener(() => setState(() {
          updateValue();
        }));
    widget.timeWindowListener.addListener(() => setState(() {
          updateValue();
        }));
  }

  void updateValue() {
    //setState(() {
    List<TransactionItem> newTransactions = List<TransactionItem>();
    List<Transaction> payments = widget.accountListener.value
        .getTransactionsBetweenDates(
            DateTime.now()
                .subtract(new Duration(days: widget.timeWindowListener.value)),
            DateTime.now());
    for (Transaction t in payments) {
      newTransactions.add(TransactionItem(t));
    }
    widget.transactionItems = newTransactions;
    //});
  }

  TabController _tabController;

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  double _calcHeight(){
    double height = widget.transactionItems.length * TransactionItem.height;
    if(true) {
      height = 1000;
    }
    return height;
  }

  @override
  Widget build(BuildContext context) {
    updateValue();

    return Column(children: <Widget>[
      TabBar(
        //isScrollable: true,
        controller: _tabController,
        tabs: myTabs,
        indicatorColor: Colors.black54,
        //labelStyle: TextStyle(color: Colors.black54),
        labelColor: Colors.black54,
      ),
      SizedBox(height: _calcHeight(),child: TabBarView(controller: _tabController, children: [
        Column(
          children: widget.transactionItems.reversed.toList(),
        ),
        SpendingOverview(widget.accountListener, widget.timeWindowListener)
      ])),
    ]);
  }
}
