import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:well_spent/backend/financeObjects/Account.dart';
import 'package:well_spent/backend/financeObjects/Currency.dart';
import 'dart:math' as math;

import 'package:well_spent/backend/financeObjects/Institution.dart';

class BankCard extends Container {
  static double width = 340;
  static double height = 200;
  Account _account = new Account(-1, "unknown account",
      currency: Currency(-1, "err", "err", "?"),
      institution: Institution(-1, "unknown bank"));
  Currency _currency;
  Institution _institution;
  Color _color;

  BankCard(Account account) {

      _account = account ?? Account.fallBack();
      _institution = _account.institution ??
          new Institution(-1, "ERROR, unknown institution");
      _currency = _account.currency ?? Currency.fallBack();
      _color = _account.color ?? Colors.red;

  }

  Account get account => _account;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: width,
        height: height,
        padding: EdgeInsets.only(left: 5, top: 40, bottom: 40, right: 5),
        child: new Card(
            color: _color,
            //Color((math.Random().nextDouble() * 0xFFFFFF).toInt() << 0).withOpacity(1.0),
            elevation: 6,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.all(new Radius.circular(25.0))),
            child: new Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  ListTile(
                    title: Text(_account.name,
                        style: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.white)),
                    subtitle: new Text(_institution.name,
                        style: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.white)),
                    leading: Icon(
                      Icons.account_balance,
                      color: Colors.white,
                    ),
                  ),
                  // Divider(),
                  /*ListTile(
                    title: Text(_accountOwner,
                        style: TextStyle(fontWeight: FontWeight.w500, color: Colors.white)),
                    leading: Icon(
                      Icons.account_circle,
                      color: Colors.white,
                    ),
                  ),*/
                  ListTile(
                    title: Text(
                        "${_currency.getAmountString(_account.balance)}",
                        style: TextStyle(
                            fontWeight: FontWeight.w500, color: Colors.white)),
                    leading: Icon(
                      Icons.account_balance_wallet,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            )));
  }
}
