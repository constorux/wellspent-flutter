import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:well_spent/UI/bankcard.dart';
import 'dart:math' as math;

import 'package:well_spent/backend/financeObjects/Account.dart';
import 'package:well_spent/components/GUtil.dart';
import 'package:well_spent/components/RConsole.dart';

class AccountSlider extends StatelessWidget {
  List<Account> accounts;
  double height = 270;
  int activeCard = 0;
  Function onPageChanged;
  Function onBankcardChange;
  List<BankCard> _bankcards = List<BankCard>();

  AccountSlider(this.accounts, {this.onPageChanged}) {
    onBankcardChange = (int pageNr){activeCard = pageNr; onPageChanged(pageNr);};
    RConsole.debug(this.runtimeType, "created");
    //RConsole.debug(this.runtimeType, "AccountSlider / created");
    for (Account account in accounts) {
      _bankcards.add(BankCard(account));
    }
  }

  List<BankCard> get bankcards => _bankcards;

  @override
  Widget build(BuildContext context) {
    RConsole.debug(this.runtimeType, "rebuilt");
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(
          // you may want to use an aspect ratio here for tablet support
          height: height,
          child: ScrollConfiguration(
        behavior: HideScrollGlowBehavior(),
    child: PageView.builder(
            onPageChanged: onBankcardChange,
            itemCount: accounts.toSet().length,
            // store this controller in a State to save the carousel scroll position
            controller: PageController(viewportFraction: BankCard.width / MediaQuery.of(context).size.width, keepPage: true, initialPage: activeCard),

            itemBuilder: (BuildContext context, int itemIndex) {
              if (_bankcards.toSet().length > 0) {
                return _bankcards.elementAt(itemIndex);
              }
            },
          )),
        )
      ],
    );
  }
}
