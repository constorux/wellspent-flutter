import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:well_spent/UI/RevenueStatistic.dart';
import 'package:well_spent/backend/financeObjects/Account.dart';
import 'package:well_spent/components/RPieChart.dart';

class SpendingOverview extends StatelessWidget {
  final ValueListenable<Account> accountListener;
  final ValueListenable<int> timeWindowListener;

  SpendingOverview(this.accountListener, this.timeWindowListener);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        new RevenueStatistic(accountListener.value, timeWindowListener.value, true),
        new RevenueStatistic(accountListener.value, timeWindowListener.value, false),
      ],
    );
  }
}
