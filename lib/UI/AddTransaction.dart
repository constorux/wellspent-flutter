import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:well_spent/backend/WSProject.dart';
import 'package:well_spent/backend/financeObjects/Account.dart';
import 'package:well_spent/backend/financeObjects/Currency.dart';
import 'package:well_spent/backend/financeObjects/Payee.dart';
import 'package:well_spent/backend/financeObjects/Transaction.dart';
import 'package:well_spent/components/GDialog.dart';
import 'package:well_spent/components/GForm/GActionForm.dart';
import 'package:well_spent/components/GForm/GFinanceObjectForm.dart';
import 'package:well_spent/components/GForm/GInputForm.dart';
import 'package:well_spent/components/GForm/GOptionsForm.dart';
import 'package:well_spent/components/GForm/controller/GValueController.dart';
import 'package:well_spent/components/RConsole.dart';
import 'package:well_spent/components/RMoneyField.dart';

class AddTransaction extends StatelessWidget {
  Account account;
  WSProject project;
  DateTime selectedDate = DateTime.now();


  AddTransaction(this.project, {this.account, Key key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    TextEditingController ctrAmount = new TextEditingController(text: "0.00");
    GValueController gvcInfo = new GValueController();
    GValueController gvcPayee = new GValueController(options: project.payees);
    GValueController gvcDate = new GValueController(value: DateTime.now(), customToStringConverter: (object) => "${(object as DateTime).day}.${(object as DateTime).month}.${(object as DateTime).year}");
    GValueController gvcAccount = new GValueController(value: account, options: project.accounts);
    GValueController gvcPaymentType = new GValueController(options: Transaction.paymentTypes);
    GValueController gvcCategory = new GValueController(options: project.categories);
    GValueController gvcStatus = new GValueController(options: Transaction.paymentStatuses);
    GValueController gvcTags = new GValueController();
    GValueController gvcMemo = new GValueController();

    TextStyle txtStyle = TextStyle(fontSize: 19);
    GValueController dateController = GValueController();

    return Scaffold(
        appBar: AppBar(
            elevation: 0,
            title: Text("Add transaction"),
            actions: <Widget>[
              // action button
              IconButton(
                icon: Icon(Icons.check),
                onPressed: () {
                  Account acc = gvcAccount.value;
                  if (acc != null) {
                    acc.addTransaction(Transaction(
                        -1,
                        double.parse(
                            ctrAmount.text.replaceAll(new RegExp("[ ]"), "")),
                        gvcInfo.valueAsString,
                        gvcPayee.value,
                        acc,
                        gvcPaymentType.value,
                        selectedDate,
                        gvcCategory.value));

                    Navigator.pop(context);
                  } else {
                    RConsole.error(this.runtimeType, "account was null");
                  }
                },
              ),
            ]),
        body: Container(
            color: Theme
                .of(context)
                .primaryColor,
            padding: EdgeInsets.only(left: 5, right: 5),
            child: new Column(children: <Widget>[
            Container(
            color: Colors.transparent,
                height: 120,
                child: Row(children: <Widget>[
                  AspectRatio(
                      aspectRatio: 1,
                      child: Center(
                          child: Icon(
                            Icons.payment,
                            color: Colors.white,
                            size: 50,
                          ))),
                  Expanded(
                    child: Container(
                        padding: EdgeInsets.only(right: 30),
                        child: RMoneyField(
                          Currency(1, "euro", "EUR", "€"),
                          controller: ctrAmount,
                        )),
                    // textAlign: TextAlign.center,
                    // style: TextStyle(fontWeight: FontWeight.w300, fontSize: 40, color: Colors.white),
                  )
                ])),
            Expanded(
                child: Container(
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(240, 240, 240, 1.0),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10.0),
                            topRight: Radius.circular(10.0))),
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    child: new SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[

                        GInputForm(title: "info", controller: gvcInfo),
                        GFinanceObjectForm(title: "payee",
                          controller: gvcPayee,),
                        GActionForm(title: "pick date", controller: gvcDate, padded: true,
                          chooseFunction: (context, controller) =>
                              showDatePicker(context: context,
                                  initialDate: controller.value ??
                                      DateTime.now(),
                                  firstDate: DateTime(1970),
                                  lastDate: DateTime(2500)),),
                            GFinanceObjectForm(title: "account",
                              controller: gvcAccount,),
                            GFinanceObjectForm(title: "paymeent type",
                              controller: gvcPaymentType,),
                            GFinanceObjectForm(title: "category",
                              controller: gvcCategory,),
                            GFinanceObjectForm(title: "status",
                              controller: gvcStatus,),
                            GInputForm(title: "tags TODO", controller: gvcTags),
                            GInputForm(title: "info", controller: gvcMemo, padded: true,),



                        ]))))
        ])));
  }
}
