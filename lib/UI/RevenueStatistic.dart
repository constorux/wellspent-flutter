import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:well_spent/backend/financeObjects/Account.dart';
import 'package:well_spent/backend/financeObjects/Transaction.dart';
import 'package:well_spent/components/RPieChart.dart';

class RevenueStatistic extends StatelessWidget {
//AccountGraph graph;
  Account account;
  int timeWindow;
  bool expenses;
  List<ChartValue> chartData;

  RevenueStatistic(this.account, this.timeWindow, this.expenses);

  //TODO This calculation does not take different currency values into account !!

  List<ChartValue> loadData() {
    List<ChartValue> loadedChartData = List<ChartValue>();
    List<Transaction> payments = account.getTransactionsBetweenDates(
        DateTime.now().subtract(new Duration(days: timeWindow)),
        DateTime.now());
    for (Transaction t in payments) {
      if (t.amount.isNegative == expenses) {
        if (loadedChartData.where((ChartValue value) => value.key == t.category.name).isNotEmpty) {
          loadedChartData
              .firstWhere((ChartValue value) => value.key == t.category.name).value += t.amount;
        } else {
          loadedChartData.add(new ChartValue(t.category.name, t.amount));
        }
      }
    }
    return loadedChartData;
  }

  @override
  Widget build(BuildContext context) {
    //updateValue();
    return Container(
        padding: EdgeInsets.all(20),
        color: Colors.white,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                expenses ? "EXPENSES" : "INCOME",
                style: TextStyle(
                    color: Colors.black54, fontWeight: FontWeight.bold),
              ),
              RPieChart(loadData(),
                color: expenses
                    ? Color.fromRGBO(33, 150, 243, 1)
                    : Color.fromRGBO(255, 102, 0, 1),
              ),
            ]));
  }
}
