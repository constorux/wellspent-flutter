import 'package:flutter/cupertino.dart';
import 'package:well_spent/UI/AccountSlider.dart';
import 'package:well_spent/UI/SearchView.dart';
import 'package:well_spent/backend/WSProject.dart';
import 'package:flutter/material.dart';
import 'package:well_spent/UI/bankcard.dart';
import 'package:well_spent/UI/ReportView.dart';
import 'package:well_spent/UI/AddTransaction.dart';
import 'package:well_spent/backend/financeObjects/Account.dart';
import 'package:well_spent/components/RConsole.dart';
import 'package:flutter/services.dart';

final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
//var _homeScreenWidgets = List<Widget>();

class HomeScreen extends StatelessWidget {
  //List<BankCard> _cards = new List<BankCard>();
  AccountSlider _accountSlider;
  ReportView _reportView;
  BuildContext _context;
  WSProject project;
  final currentAccount = new ValueNotifier(Account.fallBack());

  HomeScreen(this.project) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

    _reportView = ReportView(currentAccount);
    _accountSlider = AccountSlider(
      project.accounts,
      onPageChanged: (int pageNr) {
        onSliderChange(pageNr);
      },
    );
    onSliderChange(0);
  }



  void onSliderChange(int pageNr) {
    if ((_accountSlider != null) &&
        (_accountSlider.bankcards != null) &&
        (_accountSlider.bankcards.isNotEmpty)) {
      RConsole.debug(this.runtimeType,
          "Number of accounts: ${_accountSlider.bankcards.toSet().length}");
      BankCard card = (_accountSlider.bankcards.elementAt(pageNr));
      if (card != null) {
        currentAccount.value = card.account;
      }

      RConsole.debug(this.runtimeType, "error! cards is null");
    }
  }

  Widget build(BuildContext context) {
    //showModalDialog();
    _context = context;
    //_homeScreenWidgets.add(new ReportView(_slider));

    return new Scaffold(
      backgroundColor: Color.fromRGBO(230, 230, 230, 1),


      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: new FloatingActionButton(
        highlightElevation: 20,
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () => showWidgetPage(
            new AddTransaction(
              project,
              account: currentAccount.value,
            ),
            title: "add transaction"),
        elevation: 26,
      ),
      key: _scaffoldKey,
      //backgroundColor: Theme.of(context).primaryColor,

      body: new CustomScrollView(
          shrinkWrap: true,
          controller: ScrollController(
              keepScrollOffset: false, initialScrollOffset: -200),
          slivers: <Widget>[
            SliverAppBar(
                //pinned: true,

                backgroundColor: Colors.transparent,
                iconTheme: IconThemeData(color: Colors.black87),
                centerTitle: true,
                title: new Text(
                  "WellSpent | αlpha",
                  style: TextStyle(color: Colors.black54),
                ),
                bottom: PreferredSize(
                    child: Container(), preferredSize: Size(200, 100)),
                floating: false,
                //snap: true,
                //elevation: 10,
                expandedHeight: 400.0,
                //backgroundColor: Colors.white,
                flexibleSpace: new FlexibleSpaceBar(
                    collapseMode: CollapseMode.none,
                    background: Container(
                        //height: 200,
                        //collapseMode:CollapseMode.pin,
                        child: new Container(
                      padding: const EdgeInsets.fromLTRB(0, 70, 0, 20),
                      child: _accountSlider,
                      //height: 275.0,
                    )))),

            new SliverList(
                delegate: SliverChildListDelegate([_reportView],
                    addRepaintBoundaries: false)),

            // SliverFillRemaining(child: Container(padding: EdgeInsets.all(30),),)
          ]),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 4.0,
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.menu),
              onPressed: () => showWidgetBottomSheet(Center(child: Text("menu TODO"),)),
            ),
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () => showWidgetPage(new SearchView(project)),
            ),
          ],
        ),
      ),
    );
  }

  //methods

  void showWidgetBottomSheet(Widget content, {String title}) {
    showModalBottomSheet<void>(
        context: _context,
        builder: (BuildContext context) {
          return Container(
            child: Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: content),
          );
        });
  }

  void showWidgetDialog(Widget content, {String title}) {
    // flutter defined function
    showDialog(
      context: _context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return SimpleDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          //title: new Text(title),
          contentPadding: EdgeInsets.all(0),
          children: <Widget>[content],
        );
      },
    );
  }

  void showWidgetPage(Widget content, {String title}) {
    Navigator.push(
      _context,
      MaterialPageRoute(builder: (context) => content),
    );
  }

  List<BankCard> _loadCards() {
    List<BankCard> _cards = new List<BankCard>();

    for (Account account in project.accounts) {
      _cards.add(new BankCard(account));
    }

    return _cards;
  }
}
