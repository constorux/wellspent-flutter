import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:well_spent/UI/AccountSlider.dart';
import 'package:well_spent/UI/PaymentsOverview.dart';
import 'package:well_spent/UI/AccountGraph.dart';
import 'dart:math' as math;

import 'package:well_spent/UI/bankcard.dart';
import 'package:well_spent/backend/financeObjects/Account.dart';
import 'package:well_spent/components/RConsole.dart';
import 'package:well_spent/components/RExpandableList.dart';

class ReportView extends StatelessWidget {
  final currentAccount;

  //TODO Dirty default value handling
  final currenttimeWindow = new ValueNotifier(RExpandableList.defaultValue);

  ReportView(this.currentAccount, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Stack(
        alignment: Alignment.topCenter,
        overflow: Overflow.visible,
        children: [
          Positioned(
            top: -70.0,
            left: 10.0,
            right: 10.0,
            child: new SizedBox(
                height: 70.0,
                width: MediaQuery.of(context).size.width,
                /*child: new Image.asset(
                  'assets/graphTODO.png',
                  fit: BoxFit.fill,
                ),*/
                child: AccountGraphWidget(currentAccount, currenttimeWindow)),
          ),
          new Column(

              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                RExpandableList(
                  onValueChanged: (int i) => currenttimeWindow.value = i,
                ),
                new Container(
                    margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    child: new Container(

                        //color: Colors.white,
                        //elevation: 0,
                        margin: EdgeInsets.all(0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10.0),
                                bottomRight: Radius.circular(10.0))),
                        child: new PaymentsOverview(
                            currentAccount, currenttimeWindow)))
              ]),
        ]);
  }
}
