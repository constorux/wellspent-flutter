// import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:well_spent/backend/financeObjects/Account.dart';
import 'package:well_spent/backend/financeObjects/Transaction.dart';
import 'package:well_spent/components/RConsole.dart';

class AccountGraphWidget extends StatefulWidget {
  AccountGraph graph;
  ValueListenable<Account> accountListener;
  ValueListenable<int> timeWindowListener;

  AccountGraphWidget(this.accountListener, this.timeWindowListener);

  @override
  State<StatefulWidget> createState() {
    return AccountGraphWidgetState();
  }

  void reload() {
    //account = accountListener.value;
  }
}

class AccountGraphWidgetState extends State<AccountGraphWidget> {

  @override
  void initState(){

    widget.timeWindowListener.addListener(() => setState(() {updateValue();}));
    widget.accountListener.addListener(() => setState(() {updateValue();}));

    super.initState();
  }

  void updateValue() {
      widget.graph = AccountGraph(
            widget.accountListener.value, widget.timeWindowListener.value);
    }

  @override
  Widget build(BuildContext context) {
    updateValue();
    return CustomPaint(painter: widget.graph, child: Container());
  }
}

class AccountGraph extends CustomPainter {
  Account account;
  int timeWindow = 30;

  AccountGraph(this.account, this.timeWindow);

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();

    double height = size.height;

    List<Point> points = _calcValues(timeWindow, size, height);

    canvas.translate(0.0, 0.0);

    paint.color = Colors.white;

    /*if((account != null)&&(account.color != null)) {
      paint.color = account.color;
    }*/

    final Path path = new Path();
    final Path outline = new Path();

    path.moveTo(0.0, size.height);

    if (points.isNotEmpty) {
      Point first = points.removeAt(0);
      path.lineTo(first.x, first.y);
      outline.moveTo(first.x, first.y);

      for (Point p in points) {
        path.lineTo(p.x, p.y);
        outline.lineTo(p.x, p.y);
      }
      /*final int dividerCnst = values.length;
    for (int i = 1; i < dividerCnst; i++) {
      path.lineTo(size.width / dividerCnst * i - size.width / (dividerCnst * 2),
          _getPosition(values[i], min, factor, size));
      outline.lineTo(
          size.width / dividerCnst * i - size.width / (dividerCnst * 2),
          _getPosition(values[i], min, factor, size));
    }
    */

      path.lineTo(size.width, points.last.y);
      outline.lineTo(size.width, points.last.y);
      outline.fillType = PathFillType.nonZero;

      path.lineTo(size.width,
          size.height); // last point in bottom right corner of container

      path.close();
      canvas.drawPath(path, paint);

      Gradient gradient = new LinearGradient(
        colors: <Color>[Colors.indigo, Colors.purple, Colors.pink],
        stops: [
          0.0,
          0.7,
          1.0,
        ],
      );

      paint.shader = gradient
          .createShader(new Rect.fromLTRB(0, 0, size.width, size.height));
      paint.style = PaintingStyle.stroke;
      paint.strokeCap = StrokeCap.round;
      paint.strokeJoin = StrokeJoin.round;
      paint.strokeWidth = 5;
      canvas.drawPath(outline, paint);
    }
  }

  double _getPosition(double value, double min, double factor, Size size) {
    return size.height - ((value - min) * factor);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;

  List<Point> _calcValues(int daysDuration, Size size, double height) {
    List<Point> fallback = [Point(0, 0), Point(size.width, 0)];

    if (account != null) {
      DateTime fromDate = DateTime.now().subtract(Duration(days: daysDuration));
      double balance = account.getBalanceAt(fromDate);
      double xfactor = size.width / daysDuration;

      //RConsole.debug(this.runtimeType, "" + account.transactions.toString());
      List<Transaction> transactions =
          account.getTransactionsBetweenDates(fromDate, DateTime.now());
      if ((transactions == null) || (transactions.isEmpty)) {
        RConsole.debug(this.runtimeType, "AccountGraph / falling back");
        return fallback;
      }

      List<Point> points = List<Point>();
      points.add(Point(0, balance));

      for (Transaction transaction in transactions) {
        points.add(Point(
            transaction.date.difference(fromDate).inDays * xfactor, balance));
        balance += transaction.amount;
        points.add(Point(
            transaction.date.difference(fromDate).inDays * xfactor, balance));
      }

      double max = points.first.y;
      double min = points.first.y;

      for (Point point in points) {
        if (point.y > max) {
          max = point.y;
        }

        if (point.y < min) {
          min = point.y;
        }
      }

      double yFactor = height / (max - min);
      double paddingBottom = 10;

      for (Point point in points) {
        point.y = height - ((point.y - min) * yFactor + paddingBottom);
        //RConsole.debug(this.runtimeType, point.toString());
      }
      return points;
    }
    return fallback;
  }
}

class Point {
  double x;
  double y;

  Point(this.x, this.y);

  String toString() {
    return "x: $x, y: $y";
  }
}
