import 'package:flutter/material.dart';
import 'package:well_spent/backend/financeObjects/Transaction.dart';
import 'package:well_spent/backend/financeObjects/Currency.dart';
import 'package:well_spent/components/RConsole.dart';

class TransactionItem extends StatelessWidget {
  Transaction transaction;
  static double _height = 70;

  TransactionItem.fallBack() : this(Transaction.fallBack());

  TransactionItem(this.transaction) {
    if ((transaction == null) ||
        (transaction.amount == null) ||
        (transaction.payee == null) ||
        (transaction.currency == null)) {
      RConsole.debug(this.runtimeType, transaction.toString());
      RConsole.debug(this.runtimeType,
          "TransactionItem / given Transaction has errors or does not exist");
      transaction = Transaction.fallBack();
    }
  }

  static get height => _height;

  @override
  Widget build(BuildContext context) {
    return SizedBox(height: _height, child: Container(
      margin: EdgeInsets.all(2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(6.0)),
        color: Colors.grey[50],
      ),
      //color: Colors.grey[100],
      //width: double.infinity,
      padding: EdgeInsets.all(7),
      child: Row(
        children: <Widget>[
          Expanded(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                Text(
                  transaction.info,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w300,
                      color: Colors.black54),
                  textAlign: TextAlign.left,
                ),
                Padding(
                  padding: EdgeInsets.all(2),
                ),
                Text(
                  transaction.payee.name.toUpperCase(),
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      color: Colors.black54),
                  textAlign: TextAlign.left,
                )
              ])),
          Text(
            transaction.currency.getAmountString(transaction.amount),
            style: TextStyle(
                fontSize: 23,
                fontWeight: FontWeight.w300,
                color: Colors.black54),
          )
        ],
      ),
    ));
  }
}
