import 'package:well_spent/backend/financeObjects/Account.dart';
import 'package:well_spent/backend/financeObjects/Category.dart';
import 'package:well_spent/backend/financeObjects/Currency.dart';
import 'package:well_spent/backend/financeObjects/FinanceObject.dart';
import 'package:well_spent/backend/financeObjects/Institution.dart';
import 'package:well_spent/backend/financeObjects/Payee.dart';

import 'financeObjects/Transaction.dart';

class WSProject {
  List<Currency> currencies = List<Currency>();
  List<Institution> institutions = List<Institution>();
  List<Account> _accounts = List<Account>();
  List<Category> categories = List<Category>();
  List<Payee> payees = List<Payee>();


  WSProject();

  /* void addCurrency(Currency currency) => _currencies.add(currency);
  void addInstitution(Institution institution) => _institutions.add(institution);
  void addAccount(Account account) => _accounts.add(account);
  void addTransaction(Transaction transaction) => _transactions.add(transaction);
  void addCategory(Category category) => _categories.add(category);
  void addPayee(Payee payee) => _payees.add(payee); */

  Account getAccountForKey(int k) => FinanceObject.getObjectForKey(_accounts, k);

  Category getCategoryForKey(int k) => FinanceObject.getObjectForKey(categories, k);

  Currency getCurrencyForKey(int k) => FinanceObject.getObjectForKey(currencies, k);

  Institution getInstitutionForKey(int k) => FinanceObject.getObjectForKey(institutions, k);

  Payee getPayeeForKey(int k) => FinanceObject.getObjectForKey(payees, k);


  List<Account> get accounts => _accounts;

  //void addTransaction(Transaction t, int account)


  @override
  String toString() {
    if ((currencies != null) &&
        (institutions != null) &&
        (_accounts != null) &&
        (categories != null) &&
        (payees != null)) {
      return "WellSpent_Project" +
          "currencies" +
          currencies.toString() +
          "\r\n" +
          "accounts: " +
          _accounts.toString() +
          "\r\n" +
          "Institutions: " +
          institutions.toString() +
          "\r\n" +
          "Transactions: " +
          currencies.toString() +
          "\r\n" +
          "categories: " +
          categories.toString() +
          "\r\n" +
          "payees: " +
          payees.toString() +
          "\r\n";
    } else {
      return "the current project seems corrupt. "
          "Therefore it can not be converted to a String";
    }
  }
}
