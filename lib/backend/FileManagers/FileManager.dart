import 'dart:io';

import 'package:well_spent/backend/WSProject.dart';

abstract class FileManager{

  File file = new File("");
  List<String> supportedFileExtensions;

  FileManager({this.file});

  bool writeToFile(WSProject project); //return true on success and false on failure
  WSProject readFromFile();

  set bankingFile(File bankingFile) => file = bankingFile;

}
