//TODO
//----
//implement option to load tags

import 'dart:io';

import 'package:well_spent/backend/FileManagers/FileManager.dart';
import 'package:well_spent/backend/WSProject.dart';
import 'package:well_spent/backend/financeObjects/Currency.dart';
import 'package:well_spent/backend/financeObjects/Account.dart';
import 'package:well_spent/backend/financeObjects/Category.dart';
import 'package:well_spent/backend/financeObjects/Institution.dart';
import 'package:well_spent/backend/financeObjects/Transaction.dart';
import 'package:well_spent/backend/financeObjects/FinanceObject.dart';
import 'package:well_spent/backend/financeObjects/Payee.dart';

import 'package:xml/xml.dart' as xml;

class XhbIO extends FileManager {
  String content;

  List<String> supportedFileExtensions = ["xhb"];

  XhbIO({File file, this.content}) : super(file: file);

  WSProject readFromFile() {
    WSProject project = WSProject();
    xml.XmlDocument xmlDocument;

    if (file != null) {
      xmlDocument = xml.parse(file.readAsStringSync());
    } else if (content != null) {
      xmlDocument = xml.parse(content);
    } else {
      return project;
    }

    List<String> categories = ["cur", "account", "pay", "cat", "tag", "ope"];
    for (String category in categories) {
      for (xml.XmlElement element
      in xmlDocument.findAllElements(category).toList()) {
        if (category == "cur") {
          project.currencies.add(Currency(
              parseInt(element.getAttribute("key")),
              element.getAttribute("name"),
              element.getAttribute("iso"),
              element.getAttribute("symb"),
              valueToEUR: parseDouble(element.getAttribute("key"))));
        }

        if (category == "account") {
          project.accounts.add(Account(parseInt(element.getAttribute("key")),
              element.getAttribute("name"),
              currency: project.getCurrencyForKey(parseInt(element.getAttribute("curr"))),
              //bank: element.getAttribute("bank"),
              number: element.getAttribute("number"),
              minimum: parseInt(element.getAttribute("minimum"))));
        }

        if (category == "pay") {
          project.payees.add(Payee(
            parseInt(element.getAttribute("key")),
            element.getAttribute("name"),
          ));
        }

        if (category == "cat") {
          project.categories.add(Category(
              parseInt(element.getAttribute("key")),
              element.getAttribute("name"),
              project
                  .getCategoryForKey(parseInt(element.getAttribute("number"))),
              intToBool(parseInt(element.getAttribute("minimum")))));
        }

        if (category == "ope") {
          project.getAccountForKey(parseInt(element.getAttribute("account"))).addTransaction(Transaction(
            parseInt(element.getAttribute("key")),
            parseDouble(element.getAttribute("amount")),
            element.getAttribute("info"),
            project.getPayeeForKey(parseInt(element.getAttribute("payee"))),
            project.getAccountForKey(parseInt(element.getAttribute("account"))),
            Transaction.getPaymentTypeForKey(parseInt(element.getAttribute("paymode"))),
            daysSince111ToDate(parseInt(element.getAttribute("date"))),
            project.getCategoryForKey(parseInt(element.getAttribute("category"))),
            //getCurrencyForKey(project.currencies, int.parse(element.getAttribute("account")))
          ));
        }
      }
    }

    for (Account account in project.accounts) {
      account.orderTransactionByDates();
    }
    //RConsole.debug(this.runtimeType, project.toString());
    return project;
  }

  bool writeToFile(WSProject project) {}

  String getAttributeValue(List<xml.XmlAttribute> attributes,
      String attributeName) {
    for (xml.XmlAttribute attribute in attributes) {
      //if (attribute.name.toString() == attributeName) {
      return attribute.name.toString();
      //}
    }
  }

  int parseInt(String s) {
    if (s != null) {
      return int.parse(s);
    }
    return null;
  }

  double parseDouble(String s) {
    if (s != null) {
      return double.parse(s);
    }
    return null;
  }

  bool intToBool(int i) {
    if ((i != null) && (i > 0)) {
      return true;
    }
    return false;
  }

  final birthday = DateTime(1967, 10, 12);
  final date2 = DateTime.now();
  final difference = DateTime(0001, 1, 1)
      .difference(DateTime.now())
      .inDays;

  DateTime daysSince111ToDate(int date) {
    int today = DateTime.now()
        .difference(DateTime(0001, 1, 1))
        .inDays;
    int difference = today - date;

    if (difference > 0) {
      return DateTime.now().subtract(new Duration(days: difference));
    }


    return DateTime.now().add(new Duration(days: (difference).abs()));
  }
}
