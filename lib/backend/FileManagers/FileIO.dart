import 'dart:io';

import 'package:well_spent/backend/WSProject.dart';
import 'package:well_spent/backend/WellSpent.dart';
import 'package:well_spent/backend/FileManagers/FileManager.dart';
import 'package:well_spent/backend/FileManagers/XhbIO.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:well_spent/components/RConsole.dart';

class FileIO {
  static List<FileManager> _filemgrs = List<FileManager>();
  WellSpent _wellSpent;

  FileIO(WellSpent wellspent) {
    _wellSpent = wellspent;
    _filemgrs.add(XhbIO());
  }

  //TODO implement getter for 'real' files
  void loadFile(String fileName) {
    if((fileName.contains("."))&&(!fileName.endsWith("."))) {
      String fileExtension =
      fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length);
      RConsole.debug(this.runtimeType, "FileIO / filetype: " + fileExtension);


      for (FileManager fileManager in _filemgrs) {
        if (fileManager.supportedFileExtensions
            .contains(fileExtension.toLowerCase())) {
          getFileData(fileName).then((value) {
            _wellSpent.loadUI(XhbIO(content: value).readFromFile());
          });
          return;
        }
      }
    }
    //Future<WSProject> project =

    //return XhbIO(content: f).readFromFile();
    _wellSpent.couldNotLoadFile(fileName);
  }

  static Future<String> getFileData(String path) async {
    return await rootBundle.loadString(path);
  }
}
