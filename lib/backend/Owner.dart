class Owner {
  String _vorname; //german name for first name.
  String _nachname; //german name for last name.
  //I kind of liked the sound, so I did not change it here. If it bothers you, feel free to change it.

  Owner.fallBack() : this(null, null);

  Owner(String vorname, String nachname) {
    _vorname = vorname ?? "Default Owner";
    _nachname = nachname ?? "ERROR";
  }

  String get name => _vorname + " " + _nachname;

  String get lastName => _nachname;

  String get firstName => _vorname;
}
