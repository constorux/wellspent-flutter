import 'package:well_spent/backend/financeObjects/FinanceObject.dart';

class Institution extends FinanceObject {

  Institution.fallBack() : this(-1, null);

  Institution(int key, String bankName) : super(key, bankName ?? "Fallback institution");

  String toString() {
    return "Institution {key: $key, name: $name}";
  }
}