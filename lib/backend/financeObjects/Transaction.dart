import 'package:well_spent/backend/financeObjects/Account.dart';
import 'package:well_spent/backend/financeObjects/Currency.dart';
import 'package:well_spent/backend/financeObjects/FinanceObject.dart';
import 'package:well_spent/backend/financeObjects/Payee.dart';

import 'Category.dart';

class Transaction extends FinanceObject {


  double _amount;
  String _info;
  PaymentType _paymentType;
  Payee _payee;
  Account _account;
  Account _accountTo;
  DateTime _date;
  List<String> _tags = List<String>();
  String _memo;
  TransactionStatus _status = getTransactionStatusForKey(-1);
  Category _category;
  Currency _currency;
  double _currentExchangeRateToEUR;

  Transaction.fallBack() : this(
      -1,
      null,
      "fallback transaction",
      null,
      null,
      null,
      null,
      null);


  Transaction(int key, double amount, String info, Payee payee, Account account,
      PaymentType paymentType, DateTime date, Category category, {Currency currency,
        List<String> tagsList, String memo = "", TransactionStatus status
            , String tagsString = "", double currentExchangeRateToEUR})
      : super(key, "transaction $key") {
    _amount = amount ?? 0.00;
    _info = info ?? "unknown transaction";
    _paymentType = paymentType ?? getPaymentTypeForKey(-1);
    _date = date ?? DateTime.now();
    _tags = tagsList ?? _tagStringToList(tagsString);

    _payee = payee ?? Payee.fallBack();
    _account = account ?? Account.fallBack();
    _currency = _account.currency ?? Currency.fallBack();
    _currency = currency ?? Currency.fallBack();
    _category = category ?? Category.fallBack();

    _currentExchangeRateToEUR = currentExchangeRateToEUR ?? 0.0;
  }

  double get amount => _amount;

  String get info => _info;

  Payee get payee => _payee;

  PaymentType get paymentType => _paymentType;

  DateTime get date => _date;

  List<String> get tags => _tags;

  String get memo => memo;

  TransactionStatus get status => _status;

  Currency get currency => _currency;

  Category get category => _category;


  List<String> _tagStringToList(String tagsString) {
    List<String> tags = List<String>();
    if ((tagsString != null) && (tagsString != "")) {
      List<String> tags = tagsString.split(" ");
      if ((tags != null) && (tags.isNotEmpty)) {
        for (String tag in tags) {
          tags.add(tag);
        }
      }
    }
    return tags;
  }


  String toString() {
    return "Transaction {key: $key, amount: $_amount, info: $_info, payee: $_payee, paymentType: $_paymentType, account $_account, accountTo; $_accountTo, date $_date, currency: $_currency, tags: $_tags, memo: $_memo}";
  }









  static PaymentType getPaymentTypeForKey(int k) =>
      FinanceObject.getObjectForKey(paymentTypes, k);

  static TransactionStatus getTransactionStatusForKey(int k) =>
      FinanceObject.getObjectForKey(paymentStatuses, k);

  static List<PaymentType> paymentTypes = [
    PaymentType(-1, "error"),
    PaymentType(1, "creditCard"),
    PaymentType(2, "check"),
    PaymentType(3, "cash"),
    PaymentType(4, "transfer"),
    PaymentType(5, "internalTransfer"),
    PaymentType(6, "debitCard"),
    PaymentType(7, "standingOrder"),
    PaymentType(8, "electronicPayment"),
    PaymentType(9, "deposit"),
    PaymentType(10, "fiFee"),
    PaymentType(11, "directDebit"),
  ];


  static List<TransactionStatus> paymentStatuses = [
      TransactionStatus(-1, "error"),
      TransactionStatus( 1, "cleared"),
      TransactionStatus( 2, "reconsiled"),
      TransactionStatus( 3, "remind"),
    ];



}

class TransactionStatus extends FinanceObject {

  TransactionStatus(int key, String name) : super(key, name);

  String toString() {
    return "TransactionStatusType {key: $key, name: $name}";
  }
}


class PaymentType extends FinanceObject{

  PaymentType.fallBack() : this(-1, "Fallback payment type");

  PaymentType(int key, String name) : super(key, name ?? "unknown payment type");

  String toString()
  {
    return "PaymentType {key: $key, name: $name}";
  }

}



