import 'package:well_spent/backend/financeObjects/Account.dart';
import 'package:well_spent/backend/financeObjects/FinanceObject.dart';

class Category extends FinanceObject{
  Category _parent;
  bool _isIncome;

  Category.fallBack() : this(-1, "fallback category", null, null);

  Category(int key, String name, Category parent, bool isIncome) : super(key, name ?? "unknown category"){
    _parent = parent ?? null;
    _isIncome = isIncome ?? false;

    hasParent;
  }

  bool get isIncome => _isIncome;
  bool get hasParent {if(_parent != null){return true;}return false;}

  String toString()
  {
    return "Category {key: $key, name: $name, isIncome: $_isIncome}";
  }
}

