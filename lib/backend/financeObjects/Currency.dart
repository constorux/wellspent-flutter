import 'package:well_spent/backend/financeObjects/FinanceObject.dart';

class Currency extends FinanceObject {
  String _abbreviation;
  String _symbol;
  bool _symbolBehindValue;
  double _valueToEUR; //Euro will serve as the reference currency

  Currency.fallBack() : this(-1, "fallback currency", null, null);

  Currency(int key, String name, String abbreviation, String symbol,
      {double valueToEUR, bool symbolBehindValue}) : super(key, name ?? "unknown currency"){

    _abbreviation = abbreviation ?? "ERR";
    _symbol = symbol ?? "?";
    _symbolBehindValue = symbolBehindValue ?? true;
    _valueToEUR = valueToEUR ?? 0;

  }

  String get abbreviation => _abbreviation;

  String get symbol => _symbol;

  bool get symbolBehindValue => _symbolBehindValue;

  double get valueToEUR => _valueToEUR;

  String getAmountString(double value) {
    if (_symbolBehindValue) {
      return (value.toString() + " " + _symbol);
    }
    return (_symbol + " " + value.toString());
  }

  String toString()
  {
    return "Currency {key: $key, name: $name, abbreviation: $_abbreviation, symbol: $_symbol, symbolBehindValue $_symbolBehindValue, valueToEUR $_valueToEUR}";
  }
}
