import 'dart:ui';

import 'package:well_spent/backend/financeObjects/FinanceObject.dart';
import 'package:well_spent/backend/financeObjects/Institution.dart';
import 'package:well_spent/backend/financeObjects/Currency.dart';
import 'package:well_spent/backend/Owner.dart';
import 'package:well_spent/backend/financeObjects/Transaction.dart';
import 'dart:math' as math;

class Account extends FinanceObject {
  List<Transaction> _transactions = List<Transaction>();

  Institution _institution;
  Owner _owner;
  Currency _currency;
  String _accountNr;
  int _minimum;
  double _initialOffset = 0;
  Color _color;

  Account.fallBack()
      : this(-1, "Fallback account", color: Color.fromRGBO(255, 0, 0, 1));

  Account(int key, String name,
      {Currency currency,
      Institution institution,
      String number,
      Owner owner,
      Color color,
      int minimum})
      : super(key, name ?? "unknown account") {

    _owner = owner ?? Owner.fallBack();
    _institution = institution ?? Institution.fallBack();
    _accountNr = number ?? "0000";
    _minimum = minimum ?? 0;
    _currency = currency ?? Currency.fallBack();
    _color = color ?? Color.fromRGBO(0, 128, 128, 1);

    if (currency != null) {
      _currency = currency;
    }

    //color = Color.fromRGBO(math.Random().nextInt(255) , math.Random().nextInt(255), math.Random().nextInt(255), 1);
  }


  double get balance => _getCurrentBalance();
  Institution get institution => _institution;
  Currency get currency => _currency;
  Color get color => _color;
  String get institutionName {
    if (_institution != null) {
      return _institution.name;
    }
    return "no Institution";
  }




  void orderTransactionByDates() =>
      _transactions.sort((a, b) => a.date.compareTo(b.date));

  void addTransaction(Transaction transaction) =>
      _transactions.add(transaction);

  Transaction getTransactionForKey(int k) => getObjectForKey(_transactions, k);

  double _getCurrentBalance() {
    return getBalanceAt(DateTime.now());
  }

  double getBalanceAt(DateTime time) {
    double value = _initialOffset;
    if ((_transactions != null) && (_transactions.isNotEmpty)) {
      Transaction transaction = _transactions.first;

      for (Transaction transaction in _transactions
          .where((transaction) => transaction.date.isBefore(time))) {
        value = value + transaction.amount;
      }
    }
    return num.parse(value.toStringAsFixed(2));
  }

  List<Transaction> get transactions => _transactions;

  int getTransactionNumberDEBUG() {
    int i = 0;
    for (Transaction t in transactions) {
      i++;
    }
    return i;
  }

  List<Transaction> getTransactionsBetweenDates(DateTime first, DateTime last) {
    List<Transaction> result = List<Transaction>();
    for (Transaction transaction in _transactions) {
      if ((transaction != null) &&
          (transaction.date != null) &&
          (transaction.date.isAfter(first)) &&
          (transaction.date.isBefore(last))) {
        result.add(transaction);

        //RConsole.debug(this.runtimeType, "=>" + first.toIso8601String());
      }
    }
    return result;
  }

  String toString() {
    return "Account {key: $key, name: $name, Currency: $_currency, Institution: $_institution, AccountNumber: $_accountNr, minimum $_minimum, owner $_owner, transactions: $_transactions}";
  }

  //TODO redundancy with the method from WSProject.dart
  FinanceObject getObjectForKey(List<FinanceObject> list, int key) {
    for (FinanceObject financeObject in list) {
      if (financeObject.key == key) {
        return financeObject;
      }
    }
    return null;
  }
}
