abstract class FinanceObject {
  int key;
  String _name;

  FinanceObject(this.key, String name) {
    _name = name;
  }

  String get name => _name;

  String toString() {
    return "FinanceObject {key: $key}";
  }

  static FinanceObject getObjectForKey(List<FinanceObject> list, int key) {
    for (FinanceObject financeObject in list) {
      if (financeObject.key == key) {
        return financeObject;
      }
    }
    return null;
  }
}
