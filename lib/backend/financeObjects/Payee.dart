import 'package:well_spent/backend/financeObjects/FinanceObject.dart';

class Payee extends FinanceObject{

  Payee.fallBack() : this(-1, "Fallback payee");

  Payee(int key, String name) : super(key, name ?? "unknown payee");

  String toString()
  {
    return "Payee {key: $key, name: $name}";
  }

}