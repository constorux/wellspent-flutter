import 'package:flutter/widgets.dart';
import 'package:well_spent/backend/Owner.dart';
import 'package:well_spent/backend/financeObjects/Account.dart';
import 'package:well_spent/UI/bankcard.dart';
import 'package:well_spent/UI/homeScreen.dart';
import 'package:well_spent/components/RConsole.dart';
import 'package:well_spent/main.dart';
import 'package:well_spent/backend/FileManagers/FileIO.dart';
import 'package:well_spent/backend/WSProject.dart';
import 'dart:math' as math;

class WellSpent {
  AppMainWidget _appWidget;
  WSProject _project;

  Owner _owner;
  List<Account> _accounts = new List<Account>();

  WellSpent(AppMainWidget appWidget) {
    _appWidget = appWidget;
    _loadFile("assets/example.xhb");
    //loadUI();
  }

  void _loadFile(String fileName) {
    FileIO(this).loadFile(fileName);
  }

  void couldNotLoadFile(fileName) {
    _appWidget.showToast("could not load file at <$fileName>");
  }

  void loadUI(WSProject project) {
    if (project != null) {
      RConsole.debug(this.runtimeType, "loading UI");
      _project = project;
      HomeScreen _homeScreen = new HomeScreen(_project);
      _appWidget.showPage("mainScreen", _homeScreen, true);
    } else {
      RConsole.debug(this.runtimeType, "file could not be loaded");
    }
  }
}
