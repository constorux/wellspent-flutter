import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:well_spent/components/RConsole.dart';

import 'GOutput.dart';

class RNumberField extends TextFormField {
  int _decimalPlaces = 2;

  RNumberField({
    int decimalPlaces = 2,
    //int maxPlaces = 10,
    //String initialValue = "0.000",
    TextAlign textAlign = TextAlign.right,
    TextStyle style = const TextStyle(fontWeight: FontWeight.w300, fontSize: 40, color: Colors.black54),
    bool autofocus = false,
    TextEditingController controller,
    EdgeInsets scrollPadding = const EdgeInsets.all(20.0),
    //InputDecoration decoration = InputDecoration(border: InputBorder.none),

  }) : super(
            textAlign: textAlign,
            style: style,
            controller: controller,
            //initialValue: (DecimalTextInputFormatter(decimalRange: decimalPlaces).calculateValue().text),
            autofocus: autofocus,

            scrollPadding: scrollPadding,
            enableInteractiveSelection: false,
            decoration: new InputDecoration.collapsed(),
            inputFormatters: [
              DecimalTextInputFormatter(decimalRange: decimalPlaces)
            ],
            keyboardType: TextInputType.numberWithOptions(decimal: false, signed: true)) {
    _decimalPlaces = decimalPlaces;

  }
}

class DecimalTextInputFormatter extends TextInputFormatter {
  int decimalRange = 2;

  DecimalTextInputFormatter({this.decimalRange});

// : assert(decimalRange == null || decimalRange > 0);

  TextEditingValue calculateValue()
  {
    return formatEditUpdate(null, TextEditingValue(text: ""));
  }

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue, // unused.
    TextEditingValue newValue,
  ) {
    if (decimalRange != null) {
      String value = newValue.text;

      if (value == null) {
        value = "";
      }

      int matches = "-".allMatches(value).toList().length;
      value = value.replaceAll(new RegExp("-"), "");
      value = value.replaceAll(new RegExp(" "), "");



      value = value.replaceAll(new RegExp(r"[^=0123456789]"), "");

      if (value.length < (decimalRange + 1)) {
        for (int i = 0; i <= decimalRange; i++) {
          value =  "0" + value;
        }
      }



      String nonDecimalNumbers =
          value.substring(0, value.length - decimalRange);

      while ((nonDecimalNumbers.length > 1) &&
          (nonDecimalNumbers.startsWith(new RegExp(r"0")))) {
        nonDecimalNumbers =
            nonDecimalNumbers.substring(1, nonDecimalNumbers.length);
      }

      String newTextValue = nonDecimalNumbers +
          "." +
          value.substring(value.length - decimalRange, value.length);

      if((matches % 2 != 0)){
        newTextValue = "- " + newTextValue;
        RConsole.debug(this.runtimeType, value);
      }

      return TextEditingValue(
        text: newTextValue,
        selection: TextSelection.collapsed(offset: newTextValue.length),
        composing: TextRange.empty,
      );
    }
    return newValue;
  }
}
