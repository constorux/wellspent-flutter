import 'package:flutter/material.dart';
import 'package:well_spent/backend/financeObjects/Currency.dart';
import 'package:well_spent/components/RNumberField.dart';

class RMoneyField extends StatefulWidget {
  final Currency currency;
  TextEditingController controller;

  RMoneyField(this.currency, {double initialValue, this.controller}) {
    //TODO implement initial state
  }

  _RMoneyFieldState createState() => new _RMoneyFieldState(currency);
}

class _RMoneyFieldState extends State<RMoneyField> {
  Currency currency;

  _RMoneyFieldState(Currency currency, ) {
    this.currency = currency;
  }

  /* @override
  void initState() {
    super.initState();
  }*/

  Widget build(BuildContext context) {
    TextStyle style = TextStyle(
        fontWeight: FontWeight.w200, fontSize: 40, color: Colors.white);

    List<Widget> elements = <Widget>[];

    if (!currency.symbolBehindValue) {
      elements.add(Text(
        currency.symbol,
        style: style,
      ));

    }



    elements.add(Expanded(
      child: RNumberField(
      controller: widget.controller,
      style: style,
    )

        ));

    if (currency.symbolBehindValue) {

      elements.add(Text(
        currency.symbol,
        style: style,
      ));
    }

    return SizedBox(
        width: 300,
        child: Row(
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //mainAxisSize: MainAxisSize.min,
          children: elements,
        ));
  }
}
