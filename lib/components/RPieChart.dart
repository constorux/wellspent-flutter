import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

import 'GOutput.dart';

class RPieChart extends StatelessWidget {
  List<ChartValue> data;
  final bool animate;
  final int chartArcWidth;
  Color color;
  String dataTitle;

  RPieChart(this.data,
      {this.chartArcWidth = 22,
      this.animate = false,
      this.dataTitle = "unnamed chart",
      this.color = Colors.black});

  /*RPieChart.fallBack({Color fallbackColor})
      : this(null, 28, animate: true, color: fallbackColor);*/

  @override
  Widget build(BuildContext context) {
    List<charts.Series> dataSet = List<charts.Series>();
    dataSet.add(getDataset());

    List<FlatButton> legendStrings = List<FlatButton>();
    for (ChartValue value in data) {
      legendStrings.add(FlatButton.icon(
        onPressed: null,
        icon: new Icon(Icons.add_circle, color: value.dartColor),
        label: Expanded(
            child: Text(
          value.key,
          maxLines: 2,
          style: TextStyle(color: Colors.black),
          textAlign: TextAlign.left,
          overflow: TextOverflow.ellipsis,
        )),
      ));
    }

    return Container(
        margin: EdgeInsets.fromLTRB(5, 20, 5, 30),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  child: AspectRatio(
                      aspectRatio: 1,
                      child: Container(
                          padding: EdgeInsets.only(top: 14, right: 20),
                          child: Stack(children: <Widget>[
                            charts.PieChart(dataSet,
                                animate: animate,
                                layoutConfig: charts.LayoutConfig(
                                    leftMarginSpec:
                                        charts.MarginSpec.fixedPixel(0),
                                    topMarginSpec:
                                        charts.MarginSpec.fixedPixel(0),
                                    rightMarginSpec:
                                        charts.MarginSpec.fixedPixel(0),
                                    bottomMarginSpec:
                                        charts.MarginSpec.fixedPixel(0)),
                                defaultRenderer: new charts.ArcRendererConfig(
                                  arcWidth: chartArcWidth,
                                  minHoleWidthForCenterContent: 100,
                                )),
                            Center(
                                child: Container(
                                    padding: EdgeInsets.only(
                                        left: chartArcWidth + 1.0,
                                        right: chartArcWidth + 1.0),
                                    child: Text(
                                      "-293.84€",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w300,
                                          fontSize: 18.0,
                                          color: color),
                                      maxLines: 1,
                                      overflow: TextOverflow.fade,
                                    )))
                          ])))),
              Expanded(
                  child: Container(
                      //padding: EdgeInsets.only(left: 20),
                      child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: legendStrings,
              )))
            ]));
  }

  var fallBackData = [
    new ChartValue('value 1', 12),
    new ChartValue('value 2', 42),
    new ChartValue('value 3', 13),
    new ChartValue('value 4', 19),
  ];

  charts.Series<ChartValue, String> getDataset() {
    charts.Color currentColor =
        charts.Color(r: color.red, g: color.green, b: color.blue);

    for (ChartValue value in data) {
      if (value.chartColor == null) {
        currentColor = currentColor.lighter.lighter.lighter;
        value.chartColor = currentColor;
      }
    }

    return charts.Series(
      id: dataTitle,
      domainFn: (ChartValue value, _) => value.key,
      measureFn: (ChartValue value, _) {
        GOutput.debug("value:   $dataTitle  " + _.toString());
        return value.value;
      },
      colorFn: (ChartValue value, _) {
        GOutput.debug("color:   $dataTitle  " + _.toString());
        return value.chartColor;
      },
      data: data,
    );
  }
}

class ChartValue {
  final String key;
  double value;
  charts.Color chartColor;

  ChartValue(this.key, this.value, {this.chartColor});

  Color get dartColor =>
      Color.fromARGB(chartColor.a, chartColor.r, chartColor.g, chartColor.b);
}
