import 'package:flutter/material.dart';

class RExpandableList extends StatelessWidget {

  Function onValueChanged;
  static int defaultValue = data[0].value;
  RExpandableList({this.onValueChanged});

  @override
  Widget build(BuildContext context) {
    print("");

    return Container(
        margin: EdgeInsets.only(left: 10, right: 10, bottom: 10),
        color: Colors.white,
        child: ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) =>
              EntryItem(onValueChanged),
          itemCount: 1,
        ));
  }
}

// One entry in the multilevel list displayed by this app.
class Entry extends ListTile {
  final int value;
  final String text;
  Function _onTapFunction;

  Entry(this.text, this.value) : super(title: Text(text));


  void setOnTapAction(Function f) {
    _onTapFunction = f;
  }

  @override
  // TODO: implement onTap
  get onTap => () => _onTapFunction();
}

// The entire multilevel list displayed by this app.

List<Entry> data = <Entry>[
  Entry('last 30 days', 30),
  Entry('last 90 days', 90),
  //Entry('this week', DateTime.now().difference(DateTime(DateTime.now().year)).inDays),
  Entry('this month', DateTime.now().difference(DateTime(DateTime.now().year, DateTime.now().month)).inDays),
  Entry('this year',DateTime.now().difference(DateTime(DateTime.now().year)).inDays),
  Entry('1 day //DEBUG',1),
];

// Displays one Entry. If the entry has children then it's displayed
// with an ExpansionTile.

class EntryItem extends StatefulWidget {
  Function onValueChanged = () {};

  EntryItem(this.onValueChanged);

  @override
  EntryItemState createState() => EntryItemState();
}

class EntryItemState extends State<EntryItem> {
  Entry selected = data[0];
  double key = 0;

  void setSelected(Entry selectedEntry, {bool updateState = true}) {

    if (selectedEntry != null) {
      selected = selectedEntry;
    } else {
      selected = data.first;
    }
    widget.onValueChanged(selected.value);

    if(updateState){
      setState(() {});
    }

  }

  Widget _buildList() {

    print("rebuild");

    data
        .where((element) => (element != selected))
        .forEach((Entry e) => e.setOnTapAction(() => this.setSelected(e)));

    ExpansionTile list = ExpansionTile(title: Text(""));
     list = ExpansionTile(
      initiallyExpanded: false,
      key: new Key("${key++}"), //TODO This needs to be here in order to
                                //collabse the ExpansionTile. This appears to be
                                //a bug in Flutter
      title: Text(selected.text),
      children: data.where((element) => (element != selected)).toList(),
    );

    return list;
  }

  @override
  Widget build(BuildContext context) {
    return _buildList();
  }
}
