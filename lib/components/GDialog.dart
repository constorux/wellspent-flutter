import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'GOutput.dart';

class GDialog {
  static Future<String> showGOptionsDialog(BuildContext context, List<String> values,
      {String title = "GOptionsDialog", Function onChoose(int i)}) async {
    String wasChosen = await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.black54),
            ),
            content: Container(
              //color: Colors.greenAccent,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: values == null ? [Text("ERR: no values found")] : values.isEmpty ? [Text("no options available")] : values.map((data) =>
                      FlatButton(
                          child: Text(data),
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true).pop(data);
                          }))
                      .toList(),
                ),
              ),
            ),
          );
        });
    if((wasChosen != null)){
      return wasChosen;
    }
    return null;
  }

  static Future<String> showGInputDialog(BuildContext context,
      {String title = "GOptionsDialog", String value = ""}) async {

    TextEditingController txcInputController =
    TextEditingController(text: value ?? "");

    bool wasChosen = await showDialog<bool>(
        context: context,
        builder: (BuildContext context) {

          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.black54),
            ),
            content: Container(
              //color: Colors.greenAccent,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      TextField(
                        controller: txcInputController,
                      ),
                      MaterialButton(
                          child: Text("OK"),
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true).pop(true);
                          })
                    ]),
              ),
            ),
          );
        });
    if((wasChosen != null)&&(wasChosen)){
      return txcInputController.value.text;
    }
    return null;
  }

  static void showAlertDialog(BuildContext context,
      {String title = "GOptionsDialog",
        String message = "no message"}) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(
                title,
                style: TextStyle(color: Colors.black54),
              ),
              content: Container(
                //color: Colors.greenAccent,
                child: Text(message),
              )
          );
        });
  }
}
