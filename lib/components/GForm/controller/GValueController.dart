import 'dart:core';

class GValueController {
  var _value;
  String Function(Object object) customToStringConverter;

  List<Object> _options = List<Object>();

  List<Function> onChangeFunctions = List<Function>();

  GValueController(
      {var value, List<Object> options, this.customToStringConverter}) {
    _value = value;
    _options = options;
  }

  GValueController addCustomToStringConverter(
      String Function(Object object) function) {
    customToStringConverter = function;
    return this;
  }

  get value => _value;

  get options => _options;

  get optionsAsStrings {
    List<String> strings = List<String>();
    for (Object object in _options) {
      strings.add(object.toString());
    }
    return strings;
  }

  String get valueAsString {
    if (_value == null) {
      return null; //"no value";
    }
    if (customToStringConverter != null) {
      return customToStringConverter(value);
    }
    return _value.toString();
  }

  set value(var value) {
    _value = value;
    for (Function function in onChangeFunctions) {
      if (function != null) {
        function();
      }
    }
  }

  void addOnChangeListener(Function function) {
    if (function != null) {
      onChangeFunctions.add(function);
    }
  }
}
