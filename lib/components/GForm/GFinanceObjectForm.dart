import 'package:flutter/material.dart';
import 'package:well_spent/backend/financeObjects/FinanceObject.dart';
import 'package:well_spent/components/GDialog.dart';

import 'GActionForm.dart';
import 'controller/GValueController.dart';

class GFinanceObjectForm extends GActionForm {
  @override
  GFinanceObjectForm(
      {String title = "undefined GInputForm", GValueController controller, bool padded})
      : super(
            title: title,
            padded: padded,
            controller: controller.addCustomToStringConverter((Object o) => (o as FinanceObject ).name),
            chooseFunction: (context, controller) => showGFODialog(
                  context,
                  controller.options,
                  title: "choose option",
                ));

  GFinanceObjectForm.fallBack()
      : this(
            title: "Fallback GOptionsForm",
            controller: GValueController(value: 1, options: [
              'TV',
              'Refrigerator',
              'Mobile',
              'Tablet',
              'Smart fridge',
              'Robot',
              'Toaster',
              'Coffee maker',
              'Microwave',
              'Bread maker',
              'Computer'
            ]));


  static Future<FinanceObject> showGFODialog(BuildContext context, List<FinanceObject> values,
      {String title = "GOptionsDialog", Function onChoose(int i)}) async {
    FinanceObject wasChosen = await showDialog<FinanceObject>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.black54),
            ),
            content: Container(
              //color: Colors.greenAccent,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: values == null ? [Text("ERR: no values found")] : values.isEmpty ? [Text("no options available")] : values.map((data) =>
                      FlatButton(
                          child: Text(data.name),
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true).pop(data);
                          }))
                      .toList(),
                ),
              ),
            ),
          );
        });
    if((wasChosen != null)){
      return wasChosen;
    }
    return null;
  }

}
