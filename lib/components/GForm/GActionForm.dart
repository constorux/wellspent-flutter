import 'package:flutter/material.dart';

import 'controller/GValueController.dart';

class GActionForm extends StatefulWidget {
  String title;
  Future<Object> Function(BuildContext context, GValueController controller)
      chooseFunction = (context, controller) {};
  GValueController _controller = new GValueController();
  bool padded = false;

  String stringValue;

  GActionForm(
      {this.title,
      this.chooseFunction,
      GValueController controller,
      this.padded = false}) {
    if (controller != null) {
      _controller = controller;
    }

    stringValue = _controller.valueAsString;
  } //this.controller});

  GActionForm.fallBack() : this(title: "Fallback GCustomForm");

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _GOWState();
  }
}

class _GOWState extends State<GActionForm> {
  void _setChosenValue() {
    setState(() {
      widget.stringValue = widget._controller.valueAsString;
    });
  }

  @override
  Widget build(BuildContext context) {
    widget._controller.addOnChangeListener(() {
      _setChosenValue();
    });

    //TODO too dirty
    if (widget.padded == null) {
      widget.padded = false;
    }

    return Container(
        //color: Colors.white,
        padding: EdgeInsets.only(top: 3, bottom: 3),
        margin: EdgeInsets.only(bottom: widget.padded ? 25 : 1),
        decoration: widget.padded
            ? new BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  new BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0, 1),
                    blurRadius: 1.0,
                  ),
                ],
              )
            : new BoxDecoration(color: Colors.white),
        child: Container(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(widget.title),
                  ButtonTheme(
                      minWidth: 10.0,
                      //height: 100.0,
                      child: FlatButton(
                        onPressed: () => widget
                            .chooseFunction(context, widget._controller)
                            .then((value) {
                          widget._controller.value = value;
                        }),
                        //TODO text align to the right
                        child: Text(
                          widget.stringValue ?? "choose",
                        ),
                      ))
                ])));
  }
}

//widget.stringValue ?? "choose",
