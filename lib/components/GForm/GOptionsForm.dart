import 'package:well_spent/components/GDialog.dart';

import 'GActionForm.dart';
import 'controller/GValueController.dart';

class GOptionsForm extends GActionForm {
  @override
  GOptionsForm(
      {String title = "undefined GInputForm", GValueController controller, bool padded})
      : super(
            title: title,
            controller: controller,
            padded: padded,
            chooseFunction: (context, controller) => GDialog.showGOptionsDialog(
                  context,
                  controller.optionsAsStrings,
                  title: "choose option",
                ));

  GOptionsForm.fallBack()
      : this(
            title: "Fallback GOptionsForm",
            controller: GValueController(value: 1, options: [
              'TV',
              'Refrigerator',
              'Mobile',
              'Tablet',
              'Smart fridge',
              'Robot',
              'Toaster',
              'Coffee maker',
              'Microwave',
              'Bread maker',
              'Computer'
            ]));
}
