import 'package:well_spent/components/GDialog.dart';

import 'GActionForm.dart';
import 'controller/GValueController.dart';

class GInputForm extends GActionForm {
  GInputForm(
      {String title = "undefined GInputForm", GValueController controller, bool padded})
      : super(
            title: title,
            controller: controller,
            padded: padded,
            chooseFunction: (context, controller) => GDialog.showGInputDialog(
                context,
                title: "choose option",
                value: controller.value));

  GInputForm.fallBack() : this();
}
