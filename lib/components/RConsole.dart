//TODO this class was ported from JAVA

class RConsole {
  static bool _useANSI = false;
  static Type _lastType;
  static bool _lastTypeSensitive = false;

  static String input(String Message) {
    /*output(Thread.currentThread().getStackTrace()[1].getClass(),Message);
    System.out.println("enter input:");

    return JOptionPane.showInputDialog(Message);
    //return System.console().readLine();
    */
  }

  static void output(Type type, String message) {
    console(type, message, "033[37m");
  }

  /*static void output(String message) {
    output(Thread.currentThread().getStackTrace()[1].getClass(), Text);
  }*/

  static void debug(Type type, String Text) {
    console(type, "[DEBUG MESSAGE] " + Text, "033[36m");
  }

  static void error(Type type, String Text) {
    console(type, Text, "033[35m");
  }

  static void console(Type type, String message, String ANSI) {
    String typeString = type.toString();

    if (!_useANSI) {
      ANSI = "";
    }

    if ((_lastTypeSensitive)&&(_lastType == type)) {

      typeString = "";
    }
    _lastType = type;

    print(ANSI + _padString(typeString, pad: 30) + message);
    //System.out.println(Class.getName() +": "+Text);
  }

  /*void log(String msg) {
    if (logPath != null) {
      String Classname = Thread.currentThread().getStackTrace()[1]
          .getClassName();
      try {
        Files.write(logPath,
            (String.format("%-" + 40 + "s", Classname) + msg).getBytes(),
            StandardOpenOption.APPEND);
      }
    catch
    (
    IOException
    e) {
    //exception handling left as an exercise for the reader
    }
  }
  }*/

  static String _padString(String msg, {int pad: 0, String padChar = " "}) {
    if (msg != ""){
      padChar = "·";
      msg = msg + " ";
    }
    var paddingToAdd = pad - msg.length;
    return (paddingToAdd > 0)
        ? "$msg${new List.filled(paddingToAdd, padChar).join('')}"
        : msg;
  }
}
