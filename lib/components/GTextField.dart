
import 'package:flutter/material.dart';

class GTextField extends TextField{
  GTextField(bool rightAlignment) : super(textAlign: rightAlignment ? TextAlign.end : TextAlign.start, maxLines: 1, decoration: InputDecoration(contentPadding: EdgeInsets.all(10)));
}