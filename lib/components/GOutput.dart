class GOutput{

  static bool debugMode = true;

  static void output(String message){
    _consolePrint(message);
  }

  static void debug(String message){
    if(debugMode){
      _consolePrint("DEBUG          " + message);
    }
  }

  static void error(String message, {String reason = ""}){
    String reasonMsg = (reason != "") ? " Reason: $reason" : "";
    _consolePrint("ERROR!         " + message + reasonMsg);
  }

  static void log(String message){
    _consolePrint("LOG            " + message);
  }

  static void _consolePrint(String message){
    print(message);
  }
}
