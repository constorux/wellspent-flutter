import 'package:flutter/material.dart';

class GWidgetItem extends StatelessWidget {
  @override
  String title;
  Widget widget;
  double contentPadding;

  GWidgetItem(this.title, this.widget, {this.contentPadding = 0});

  GWidgetItem.fallBack()
      : this(
            "fallback widget",
            MaterialButton(
              child: Text("widget"),
              onPressed: () => null,
            ));

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(constraints: BoxConstraints(maxWidth: 500),child: Card(
          color: Colors.white70,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.only(
                         left: 10, right: 10, top: 10, bottom: 10),
                      child: Text(title)),
                  //ConstrainedBox(constraints: BoxConstraints.loose(Size(200, 50)), child: (child: widget),),
                 Flexible(child: ConstrainedBox(constraints: BoxConstraints(maxWidth: 200),child: Container(color: Colors.white, width: 200, height: 50,constraints: BoxConstraints.expand(), padding: EdgeInsets.all(contentPadding),child: widget)))
                ])));
  }
}
