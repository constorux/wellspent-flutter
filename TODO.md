TODO
====
- Creating a view that gives the user an overview about their current spending
    - implementing graphs
- Implementing a backend to store project files on a server to be able to share a project between different devices
- Implementing a setup system for the user to create a new project
- Adding file interpreters for GNUCash and Scrooge
    - finding a way to have GNUCash ignore tags only used by WellSpent
    
